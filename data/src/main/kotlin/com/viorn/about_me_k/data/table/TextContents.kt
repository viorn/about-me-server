package com.viorn.about_me_k.data.table

import org.jetbrains.exposed.sql.Table

internal object TextContents: Table() {
    val key = varchar("key", 100).primaryKey()
    val value = text("value")
}