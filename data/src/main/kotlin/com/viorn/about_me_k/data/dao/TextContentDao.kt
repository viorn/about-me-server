package com.viorn.about_me_k.data.dao

import com.viorn.about_me_k.data.entity.TextContent

interface TextContentDao {
    fun insertOrUpdate(textContent: TextContent)
    fun get(key: String): TextContent?
}