package com.viorn.about_me_k.data.dao

import com.viorn.about_me_k.data.entity.App

interface AppsDao {
    fun insert(app: App): App
    fun get(offset: Int, limit: Int?): List<App>
    fun getById(id: Int): App?
    fun del(id: Int)
    fun update(app: App): App?
}