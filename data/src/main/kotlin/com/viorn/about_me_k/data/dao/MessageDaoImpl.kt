package com.viorn.about_me_k.data.dao

import com.viorn.about_me_k.data.entity.Message
import com.viorn.about_me_k.data.table.Messages
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

internal class MessageDaoImpl(val database: Database) : MessageDao {
    override fun addMessage(message: Message): Message {
        return transaction(database) {
            val nMessage = com.viorn.about_me_k.data.table.Messages.insert {
                it[sender] = message.sender!!
                it[destination] = message.destination
                it[Messages.message] = message.message
            }
            message.id = nMessage[Messages.id]
            return@transaction message
        }
    }

    override fun get(sender: Int, destination: Int, offset: Int, limit: Int): List<Message> {
        return transaction(database) {
            return@transaction Messages.select { (Messages.sender eq sender) and (Messages.destination eq destination) }
                    .limit(limit, offset)
                    .map {
                        Message(it[Messages.id],
                                it[Messages.sender],
                                it[Messages.destination],
                                it[Messages.message],
                                it[Messages.date])
                    }
        }
    }
}