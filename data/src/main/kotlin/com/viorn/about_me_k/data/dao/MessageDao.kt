package com.viorn.about_me_k.data.dao

import com.viorn.about_me_k.data.entity.Message

interface MessageDao {
    fun get(sender: Int, destination: Int, offset: Int, limit: Int): List<Message>
    fun addMessage(message: Message): Message
}