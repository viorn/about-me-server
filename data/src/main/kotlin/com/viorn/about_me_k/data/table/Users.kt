package com.viorn.about_me_k.data.table

import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.Table

internal object Users : Table() {
    val id = integer("id").primaryKey().autoIncrement()
    val username = varchar("username", length = 200).uniqueIndex()
    val password = varchar("password", length = 200)
    val accessToken = varchar("access_token", length = 200).nullable().index()
    val refreshToken = varchar("refresh_token", length = 200).nullable()
    val chatToken = varchar("chat_token", length = 200).nullable()
    val expirationDate = long("expirationDate").nullable()
    val role = varchar("role", length = 30)
}