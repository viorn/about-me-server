package com.viorn.about_me_k.data.table

import org.jetbrains.exposed.sql.Table

internal object Apps : Table() {
    val id = integer("id").primaryKey().autoIncrement()
    val name = varchar("name", 60)
    val image = varchar("image", 500).nullable()
    val link = varchar("link", 500)
    var description = text("description")
}