package com.viorn.about_me_k.data.dao

import com.viorn.about_me_k.data.entity.User

interface UserDao {
    fun insert(vararg users: User)
    fun setChatToken(id: Int, token: String)
    fun getUserByIdAndChatToken(id: Int, chatToken: String): User?
    fun getUserByAccessToken(accessToken: String): User?
    fun refreshToken(accessToken: String, refreshToken: String): User?
    fun getUserByNameAndPassword(username: String, password: String): User?
}