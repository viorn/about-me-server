package com.viorn.about_me_k.data.table

import org.jetbrains.exposed.sql.Table
import java.util.*

internal object Messages : Table() {
    val id = integer("id").primaryKey()
    val sender = integer("sender").index()
    val destination = integer("destination").index()
    val message = text("message")
    val date = long("date")
}