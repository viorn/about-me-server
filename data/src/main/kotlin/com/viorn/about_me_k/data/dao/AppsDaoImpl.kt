package com.viorn.about_me_k.data.dao

import com.viorn.about_me_k.data.entity.App
import com.viorn.about_me_k.data.table.Apps
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

internal class AppsDaoImpl(val database: Database) : AppsDao {
    override fun getById(id: Int): App? {
        return transaction(database) {
            return@transaction Apps.select { Apps.id eq id }
                    .limit(1).map {
                        App(id = it[Apps.id],
                                name = it[Apps.name],
                                link = it[Apps.link],
                                image = it[Apps.image],
                                description = it[Apps.description])
                    }.firstOrNull()
        }
    }

    override fun update(app: App): App? {
        return transaction(database) {
            val id = app.id ?: return@transaction null
            Apps.update({ Apps.id eq id }, 1, { table ->
                app.name?.let { table[name] = it }
                app.image?.let { table[image] = it }
                app.link?.let { table[link] = it }
                app.description?.let { table[description] = it }
            })
            return@transaction getById(app.id ?: -1)
        }
    }

    override fun del(id: Int) {
        transaction(database) {
            Apps.deleteWhere { Apps.id eq id }
        }
    }

    override fun get(offset: Int, limit: Int?): List<App> {
        val list = ArrayList<App>()
        transaction(database) {
            val query = Apps.selectAll()
            if (limit != null)
                query.limit(limit, offset)
            query.forEach {
                list.add(App(
                        id = it[Apps.id],
                        name = it[Apps.name],
                        link = it[Apps.link],
                        image = it[Apps.image]
                ))
            }
        }
        return list
    }

    override fun insert(app: App): App {
        return transaction(database) {
            val nApp = Apps.insert {
                it[name] = app.name!!
                it[image] = app.image
                it[link] = app.link!!
                it[description] = app.description ?: ""
            }
            app.id = nApp[Apps.id]!!
            return@transaction app
        }
    }
}