package com.viorn.about_me_k.data

import com.viorn.about_me_k.common.hash
import com.viorn.about_me_k.data.dao.*
import com.viorn.about_me_k.data.entity.User
import com.viorn.about_me_k.data.table.Apps
import com.viorn.about_me_k.data.table.Messages
import com.viorn.about_me_k.data.table.TextContents
import com.viorn.about_me_k.data.table.Users
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import java.util.*

public val dataModule = Kodein.Module {
    bind<Database>() with singleton {
        val database = Database.connect("jdbc:h2:~/aboutme", driver = "org.h2.Driver")
        transaction(database) {
            SchemaUtils.create(Users, Apps, TextContents, Messages)
            if (Users.selectAll().count() == 0) {
                UserDaoImpl(database).insert(
                        User(username = "admin", password = "qwerty50".hash("SHA-1"),
                                role = "ADMIN", chatToken = UUID.randomUUID().toString()),
                        User(username = "guest", password = "qwerty10".hash("SHA-1"),
                                role = "GUEST", chatToken = UUID.randomUUID().toString())
                )
            }
        }
        return@singleton database
    }
    bind<UserDao>() with provider { UserDaoImpl(instance()) }
    bind<AppsDao>() with provider { AppsDaoImpl(instance()) }
    bind<TextContentDao>() with provider { TextContentDaoImpl(instance()) }
    bind<MessageDao>() with provider { MessageDaoImpl(instance()) }
}