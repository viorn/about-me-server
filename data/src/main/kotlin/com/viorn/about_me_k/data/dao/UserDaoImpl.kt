package com.viorn.about_me_k.data.dao

import com.viorn.about_me_k.common.hash
import com.viorn.about_me_k.data.entity.User
import com.viorn.about_me_k.data.table.Users
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant
import java.util.*
import java.util.Date

internal class UserDaoImpl(val database: Database) : UserDao {
    override fun getUserByNameAndPassword(username: String, password: String): User? {
        return transaction(database) {
            val user = com.viorn.about_me_k.data.table.Users.select { Users.username eq username }
                    .filter {
                        it[Users.password] == password.hash("SHA-1")
                    }
                    .map {
                        User(
                                id = it[Users.id],
                                username = it[Users.username],
                                role = it[Users.role]
                        )
                    }.firstOrNull()
            if (user != null) {
                user.accessToken = UUID.randomUUID().toString()
                user.refreshToken = UUID.randomUUID().toString()
                user.expirationDate = Date.from(Instant.now().plusSeconds(180))
                com.viorn.about_me_k.data.table.Users.update(
                        { Users.id eq user.id },
                        1,
                        {
                            it[accessToken] = user.accessToken
                            it[refreshToken] = user.refreshToken
                            it[expirationDate] = user.expirationDate?.time
                        })
            }
            return@transaction user
        }
    }

    override fun refreshToken(accessToken: String, refreshToken: String): User? {
        return transaction(database) {
            val user = Users.select { Users.accessToken eq accessToken }
                    .filter { it[Users.refreshToken] == refreshToken }
                    .map {
                        User(
                                id = it[Users.id],
                                username = it[Users.username],
                                role = it[Users.role]
                        )
                    }.firstOrNull()
            if (user != null) {
                user.accessToken = UUID.randomUUID().toString()
                user.refreshToken = UUID.randomUUID().toString()
                user.expirationDate = Date.from(Instant.now().plusSeconds(180))
                Users.update(
                        { Users.id eq user.id },
                        1,
                        {
                            it[Users.accessToken] = user.accessToken
                            it[Users.refreshToken] = user.refreshToken
                            it[expirationDate] = user.expirationDate?.time
                        })
            }
            return@transaction user
        }
    }

    override fun getUserByAccessToken(accessToken: String): User? {
        return transaction(database) {
            return@transaction Users.select { Users.accessToken eq accessToken }
                    .map {
                        val dateLong = it[Users.expirationDate]
                        val date = if (dateLong != null) Date(dateLong) else null
                        User(
                                id = it[Users.id],
                                username = it[Users.username],
                                role = it[Users.role],
                                expirationDate = date
                        )
                    }.firstOrNull()
        }
    }

    override fun getUserByIdAndChatToken(id: Int, chatToken: String): User? {
        return transaction(database) {
            return@transaction Users.select { (Users.id eq id) and (Users.chatToken eq chatToken) }
                    .map {
                        User(
                                id = it[Users.id],
                                username = it[Users.username],
                                role = it[Users.role]
                        )
                    }.firstOrNull()
        }
    }

    override fun setChatToken(id: Int, token: String) {
        transaction(database) {
            Users.update({
                Users.id eq id
            }, 1, {
                it[Users.chatToken] = token
            })
        }
    }

    override fun insert(vararg users: User) {
        transaction(database) {
            users.forEach { user ->
                Users.insert { users ->
                    users[username] = user.username
                    users[password] = user.password!!
                    users[role] = user.role
                    users[chatToken] = user.chatToken
                }
            }
        }
    }
}