package com.viorn.about_me_k.data.dao

import com.viorn.about_me_k.data.entity.TextContent
import com.viorn.about_me_k.data.table.TextContents
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

internal class TextContentDaoImpl(val database: Database) : TextContentDao {
    override fun insertOrUpdate(content: TextContent) {
        transaction(database) {
            if (com.viorn.about_me_k.data.table.TextContents.select { TextContents.key eq content.key }.firstOrNull() == null) {
                com.viorn.about_me_k.data.table.TextContents.insert {
                    it[key] = content.key
                    it[value] = content.value
                }
                Unit
            } else {
                com.viorn.about_me_k.data.table.TextContents.update({
                    TextContents.key eq content.key
                }, 1, {
                    it[value] = content.value
                })
                Unit
            }
        }
    }

    override fun get(key: String): TextContent? {
        return transaction(database) {
            return@transaction TextContents.select {
                TextContents.key eq key
            }.map { TextContent(key = it[TextContents.key], value = it[TextContents.value]) }
                    .firstOrNull()
        }
    }
}