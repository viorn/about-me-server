package com.viorn.about_me_k.data.entity

import java.util.*

data class Message(
        var id: Int? = null,
        var sender: Int?,
        val destination: Int,
        val message: String,
        val date: Long = Date().time
)