package com.viorn.about_me_k.data.entity

data class TextContent(
        val key: String,
        var value: String
)