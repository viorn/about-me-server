package com.viorn.about_me_k.data.entity

import java.util.*

data class User(
        var id: Int = -1,
        var username: String,
        var password: String? = null,
        var role: String,
        var accessToken: String? = null,
        var refreshToken: String? = null,
        var chatToken: String? = null,
        var expirationDate: Date? = null
)
