package com.viorn.about_me_k.data.entity

data class App(
        var id: Int? = null,
        var name: String? = null,
        var image: String? = null,
        var link: String? = null,
        var description: String? = null
)