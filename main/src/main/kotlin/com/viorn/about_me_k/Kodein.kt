package com.viorn.about_me_k

import com.viorn.about_me_k.facades.apps.AppFacade
import com.viorn.about_me_k.facades.auth.AuthFacade
import com.viorn.about_me_k.facades.facadeModule
import com.viorn.about_me_k.facades.message.MessageFacade
import com.viorn.about_me_k.facades.textcontent.TextContentFacade
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

val kodein = Kodein{
    import(facadeModule)
}

val textContentFacade by kodein.instance<TextContentFacade>()
val authFacade by kodein.instance<AuthFacade>()
val appFacade by kodein.instance<AppFacade>()
val messageFacade by kodein.instance<MessageFacade>()