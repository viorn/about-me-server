package com.viorn.about_me_k

import com.viorn.about_me_k.routes.*
import com.viorn.about_me_k.secure.roleFilter
import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.Compression
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.gson.gson
import io.ktor.http.HttpMethod
import io.ktor.locations.Locations
import io.ktor.response.header
import io.ktor.routing.route
import io.ktor.routing.routing
import java.text.DateFormat

fun Application.restApiModule() {
    install(DefaultHeaders)
    install(Compression)
    install(CallLogging)
    install(Locations)
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }
    routing {
        route("/api") {
            intercept(ApplicationCallPipeline.Call) {
                call.response.header("Access-Control-Allow-Origin", "*")
            }
            route("/admin") {
                roleFilter("ADMIN")
                route("/app", method = HttpMethod.Post) { addApp() }
                route("/app", method = HttpMethod.Put) { updateApp() }
                route("/app") { delApp() }
                route("/textContent") { setText() }
            }
            route("/private") {
                roleFilter()
                route("/chatToken", method = HttpMethod.Get) { getChatToken() }
            }
            route("/public") {
                route("/accessToken", method = HttpMethod.Post) { getAccessToken() }
                route("/apps", method = HttpMethod.Get) { getApps() }
                route("/textContent") { getText() }
            }
        }
    }
}
