package com.viorn.about_me_k

import com.viorn.about_me_k.data.entity.User
import io.ktor.util.AttributeKey

val userAttributeKey = AttributeKey<User>("user")