package com.viorn.about_me_k

import com.google.gson.Gson
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.engine.sslConnector
import io.ktor.server.netty.Netty
import io.ktor.util.generateCertificate
import java.io.File
import java.security.KeyStore

fun main(args: Array<String>) {
    var jks: KeyStore?
    val jksFile = File("build/temporary.jks").apply {
        parentFile.mkdirs()
    }

    jks = generateCertificate(jksFile) // Generates the certificate

    val env = applicationEngineEnvironment {
        module {
            restApiModule()
            webSocketModule()
        }
        connector {
            host = "0.0.0.0"
            port = 8080
        }
        sslConnector(keyStore = jks,
                keyAlias = "mykey",
                keyStorePassword = { "changeit".toCharArray() },
                privateKeyPassword = { "changeit".toCharArray() }
        ) {
            host = "0.0.0.0"
            port = 8081
        }
    }
    embeddedServer(Netty, env).start(true)
}

val gson = Gson()