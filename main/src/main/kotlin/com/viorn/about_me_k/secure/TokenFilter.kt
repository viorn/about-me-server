package com.viorn.about_me_k.secure

import com.viorn.about_me_k.authFacade
import com.viorn.about_me_k.userAttributeKey
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.header
import io.ktor.response.respond
import io.ktor.routing.Route
import java.util.*

fun Route.roleFilter(role: String? = null) {
    intercept(ApplicationCallPipeline.Call) {
        val token = call.request.header("Token")
        if (!call.request.header("Token").isNullOrEmpty()) {
            val user = authFacade.getUser(accessToken = token)
            user?.let { user ->
                if (role != null && user.role != role) call.respond(HttpStatusCode.Forbidden)
                if (user.expirationDate?.before(Date()) == true) {
                    call.respond(HttpStatusCode.Forbidden, "token expired")
                }
                call.attributes.put(key = userAttributeKey, value = user)
                return@intercept
            }
        }
        call.respond(HttpStatusCode.Forbidden)
    }
}