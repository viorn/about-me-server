package com.viorn.about_me_k.routes

import com.viorn.about_me_k.appFacade
import com.viorn.about_me_k.data.entity.App
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.locations.Location
import io.ktor.locations.delete
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route

@Location("/{id}")
class AppsLocation(val id: Int)

fun Route.getApps() {
    handle {
        if (call.response.status() != null) return@handle
        call.respond(appFacade.getApps())
    }
}

fun Route.addApp() {
    handle {
        if (call.response.status() != null) return@handle
        val app = appFacade.addApp(call.receive(App::class))
        if(app.name==null || app.link == null) {
            call.respond(HttpStatusCode.BadRequest, "Not found name and ling")
            return@handle
        }
        call.respond(app)
    }
}

fun Route.updateApp() {
    handle {
        if (call.response.status() != null) return@handle
        val app = appFacade.updateApp(call.receive(App::class))!!
        if (app.id != null)
            call.respond(app)
        else {
            call.respond(HttpStatusCode.BadRequest, "Not found id")
        }
    }
}

fun Route.delApp() {
    delete<AppsLocation> {
        if (call.response.status() != null) return@delete
        appFacade.delApp(it.id)
        call.respond(HttpStatusCode.OK)
    }
}