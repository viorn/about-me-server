package com.viorn.about_me_k.routes

import com.viorn.about_me_k.data.entity.TextContent
import com.viorn.about_me_k.textContentFacade
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.locations.put
import io.ktor.request.receiveParameters
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.Route

@Location("/{key}")
class TextContentLocation(val key: String)

fun Route.getText() {
    get<TextContentLocation> {
        if (call.response.status() != null) return@get
        val content = textContentFacade.getTextContent(it.key)
        if (content != null) {
            call.respond(content.value)
        } else {
            call.respond(HttpStatusCode.NoContent)
        }
    }
}

fun Route.setText() {
    put<TextContentLocation> {
        if (call.response.status() != null) return@put
        val value = call.receiveText()
        textContentFacade.putTextContent(TextContent(
                key = it.key,
                value = value
        ))
        call.respond(HttpStatusCode.OK)
    }
}