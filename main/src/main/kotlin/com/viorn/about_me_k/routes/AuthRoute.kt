package com.viorn.about_me_k.routes

import com.viorn.about_me_k.authFacade
import com.viorn.about_me_k.facades.auth.AuthFacade
import com.viorn.about_me_k.kodein
import com.viorn.about_me_k.userAttributeKey
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.routing.Route
import org.kodein.di.generic.instance

fun Route.getAccessToken() {
    handle {
        if (call.response.status() != null) return@handle
        val par = call.receiveParameters()
        val accessToken = par["accessToken"]
        val refreshToken = par["refreshToken"]
        val user = if (accessToken != null && refreshToken != null) {
            authFacade.refreshToken(accessToken, refreshToken)
        } else {
            authFacade.createAccessTokenByNameAndPass(par["username"], par["password"])
        }

        if (user != null) {
            call.respond(user)
        } else {
            call.respond(HttpStatusCode.Unauthorized)
        }
    }
}

fun Route.getChatToken() {
    handle {
        if (call.response.status() != null) return@handle
        val token = authFacade.newChatToken(call.attributes[userAttributeKey].id)
        call.respond(token)
    }
}