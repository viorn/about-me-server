package com.viorn.about_me_k

import com.viorn.about_me_k.data.entity.Message
import com.viorn.about_me_k.data.entity.User
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.routing.routing
import io.ktor.websocket.WebSocketServerSession
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.experimental.channels.consumeEach
import java.time.Duration

data class Auth(val id: Int, val token: String)

private object AuthorizedWSManager {
    private val authorizedUserToSessionMap = HashMap<Int, WebSocketServerSession>()
    private val authorizedSessionToUserMap = HashMap<WebSocketServerSession, Int>()

    @Synchronized
    fun put(id: Int, session: WebSocketServerSession) {
        authorizedSessionToUserMap.remove(session)
        authorizedUserToSessionMap.remove(id)
        authorizedSessionToUserMap[session] = id
        authorizedUserToSessionMap[id] = session
    }

    @Synchronized
    fun get(id: Int): WebSocketServerSession? = authorizedUserToSessionMap[id]

    @Synchronized
    fun delete(id: Int) {
        authorizedSessionToUserMap.remove(authorizedUserToSessionMap[id])
        authorizedUserToSessionMap.remove(id)
    }

    @Synchronized
    fun delete(session: WebSocketServerSession) {
        authorizedUserToSessionMap.remove(authorizedSessionToUserMap[session])
        authorizedSessionToUserMap.remove(session)
    }
}

fun Application.webSocketModule() {
    install(WebSockets) {
        pingPeriod = Duration.ofMinutes(1)
    }
    routing {
        webSocket("/ws") {
            try {
                var user: User? = null
                incoming.consumeEach {
                    if (it is Frame.Text) {
                        val message = it.readText()
                        if (message.startsWith("auth ")) {
                            val authText = message.drop(5)
                            val auth = gson.fromJson(authText, Auth::class.java)
                            user = authFacade.chatAuth(auth.id, auth.token)
                            user?.let {
                                send(Frame.Text("auth_true ${gson.toJson(user)}"))
                                AuthorizedWSManager.put(it.id, this)
                            } ?: send(Frame.Text("auth_false "))
                        }
                        if (user == null) return@consumeEach
                        user?.let { user ->
                            if (message.startsWith("send ")) {
                                val messageText = message.drop(5)
                                var message = gson.fromJson(messageText, Message::class.java)
                                message.sender = user.id
                                val destination = AuthorizedWSManager.get(message.destination)
                                message = messageFacade.addMessage(message)
                                this.send(Frame.Text("message ${gson.toJson(message)}"))
                                destination?.send(Frame.Text("message ${gson.toJson(message)}"))
                            }
                        }
                    }
                }
            } finally {
                AuthorizedWSManager.delete(this)
            }
        }
    }
}