package com.viorn.about_me_k.facades.apps

import com.viorn.about_me_k.data.entity.App

public interface AppFacade {
    suspend fun addApp(app: App): App
    suspend fun delApp(id: Int)
    suspend fun updateApp(app: App): App?
    suspend fun getApps(offset: Int = 0, limit: Int? = null): List<App>
}