package com.viorn.about_me_k.facades.textcontent

import com.viorn.about_me_k.data.dao.TextContentDao
import com.viorn.about_me_k.data.entity.TextContent
import kotlinx.coroutines.experimental.async

internal class TextContentFacadeImpl(val textContentDao: TextContentDao) : TextContentFacade {
    override suspend fun putTextContent(content: TextContent) {
        return async { textContentDao.insertOrUpdate(content) }.await()
    }

    override suspend fun getTextContent(key: String): TextContent? {
        return async { textContentDao.get(key) }.await()
    }
}