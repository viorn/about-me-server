package com.viorn.about_me_k.facades.apps

import com.viorn.about_me_k.data.dao.AppsDao
import com.viorn.about_me_k.data.entity.App
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch

internal class AppFacadeImpl(val appsDao: AppsDao) : AppFacade {
    override suspend fun updateApp(app: App): App? {
        return async { appsDao.update(app) }.await()
    }

    override suspend fun delApp(id: Int) {
        launch { appsDao.del(id) }.join()
    }

    override suspend fun getApps(offset: Int, limit: Int?): List<App> {
        return async { appsDao.get(offset, limit) }.await()
    }

    override suspend fun addApp(app: App): App {
        return async { appsDao.insert(app) }.await()
    }
}