package com.viorn.about_me_k.facades.textcontent

import com.viorn.about_me_k.data.entity.TextContent

interface TextContentFacade {
    suspend fun putTextContent(content: TextContent)
    suspend fun getTextContent(key: String): TextContent?
}