package com.viorn.about_me_k.facades.auth

import com.viorn.about_me_k.data.dao.UserDao
import com.viorn.about_me_k.data.entity.User
import kotlinx.coroutines.experimental.async
import java.util.*

internal class AuthFacadeImpl(val userDao: UserDao) : AuthFacade {
    override suspend fun newChatToken(id: Int): String {
        return async {
            val token = UUID.randomUUID().toString()
            userDao.setChatToken(id, token)
             token
        }.await()
    }

    override suspend fun chatAuth(id: Int, chatToken: String): User? {
        return async { userDao.getUserByIdAndChatToken(id = id, chatToken = chatToken) }.await()
    }

    override suspend fun getUser(accessToken: String?): User? {
        return async {
            var user: User? = null
            if (accessToken != null) {
                user = userDao.getUserByAccessToken(accessToken)
            }
            user
        }.await()
    }

    override suspend fun refreshToken(accessToken: String?, refreshToken: String?): User? {
        return async {
            var user: User? = null
            if (accessToken != null && refreshToken != null) {
                user = userDao.refreshToken(accessToken, refreshToken)
            }
            user
        }.await()
    }

    override suspend fun createAccessTokenByNameAndPass(username: String?, password: String?): User? {
        return async {
            var user: User? = null
            if (username != null && password != null)
                user = userDao.getUserByNameAndPassword(username, password)
            user
        }.await()
    }
}