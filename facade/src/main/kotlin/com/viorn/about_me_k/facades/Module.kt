package com.viorn.about_me_k.facades

import com.viorn.about_me_k.data.dataModule
import com.viorn.about_me_k.facades.apps.AppFacade
import com.viorn.about_me_k.facades.apps.AppFacadeImpl
import com.viorn.about_me_k.facades.auth.AuthFacade
import com.viorn.about_me_k.facades.auth.AuthFacadeImpl
import com.viorn.about_me_k.facades.message.MessageFacade
import com.viorn.about_me_k.facades.message.MessageFacadeImpl
import com.viorn.about_me_k.facades.textcontent.TextContentFacade
import com.viorn.about_me_k.facades.textcontent.TextContentFacadeImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider


public val facadeModule = Kodein.Module {
    import(dataModule)
    bind<AppFacade>() with provider { AppFacadeImpl(instance()) }
    bind<AuthFacade>() with provider { AuthFacadeImpl(instance()) }
    bind<MessageFacade>() with provider { MessageFacadeImpl(instance()) }
    bind<TextContentFacade>() with provider { TextContentFacadeImpl(instance()) }
}

