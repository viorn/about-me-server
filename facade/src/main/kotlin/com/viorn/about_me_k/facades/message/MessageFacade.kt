package com.viorn.about_me_k.facades.message

import com.viorn.about_me_k.data.entity.Message

public interface MessageFacade {
    fun addMessage(message: Message): Message
    fun getMessages(sender: Int,
                    destination: Int,
                    offset: Int = 0,
                    limit: Int = 50): List<Message>
}