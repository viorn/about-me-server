package com.viorn.about_me_k.facades.message

import com.viorn.about_me_k.data.dao.MessageDao
import com.viorn.about_me_k.data.entity.Message

internal class MessageFacadeImpl(val messageDao: MessageDao) : MessageFacade {
    override fun getMessages(sender: Int, destination: Int, offset: Int, limit: Int): List<Message> {
        return messageDao.get(sender, destination, offset, limit)
    }

    override fun addMessage(message: Message): Message {
        return messageDao.addMessage(message)
    }
}