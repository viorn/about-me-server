package com.viorn.about_me_k.facades.auth

import com.viorn.about_me_k.data.entity.User

public interface AuthFacade {
    suspend fun createAccessTokenByNameAndPass(username: String?, password: String?): User?
    suspend fun refreshToken(accessToken: String?, refreshToken: String?): User?
    suspend fun getUser(accessToken: String?): User?
    suspend fun chatAuth(id: Int, chatToken: String): User?
    suspend fun newChatToken(id: Int): String
}
